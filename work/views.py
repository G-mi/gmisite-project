from django.shortcuts import render
from .models import Work

def home(request):
    work = Work.objects
    return render(request, 'work/home.html', {'works':work})
