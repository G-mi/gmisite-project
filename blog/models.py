from django.db import models
from django.utils import timezone


class Post(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    pud_date = models.DateTimeField(default=timezone.now)
    image = models.ImageField()

    def __str__(self):
        return self.title

    def summary(self):
        return self.body[:100]
