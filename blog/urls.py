from django.urls import path

from . import views

urlpatterns = [
    path('', views.blogdisplay, name='blogdisplay'),
    path('<int:blog_id>/', views.fullpost, name='fullpost'),
]
