from django.shortcuts import render, get_object_or_404
from .models import Post

def blogdisplay(request):
    post = Post.objects
    return render(request, 'blog/blogdisplay.html', {'posts':post})

def fullpost(request, blog_id):
    fullpost = get_object_or_404 (Post, pk=blog_id)
    return render(request, 'blog/fullpost.html', {'post':fullpost})
